local obj = {}

obj.tileSize = nil

--Sets the tile size used by various functions
obj.setTileSize = function(tileSize)
    obj.tileSize = tileSize
end

--[[ 
From an MTE object, creates a grid where 0 is a non-solid tile, 1 is a solid 
tile e.g.

	{0,0,0,0,0,0},
	{0,1,1,0,0,0},
	{0,1,1,0,0,1},
	{1,1,0,1,0,1},
	{0,0,0,1,0,1},
	{0,0,1,1,1,1}
        
Important: the grid is derived from the Tiled layer with a property "name" set to 
"solid". Each tile within this layer must also have property "solid" set to 
"true".
]]
obj.createGridFromMTE = function(mte)
    mteMap = mte.getMap()
    local grid = {}
    for i = 1, mteMap.height, 1 do
        grid[i] = {}
        for j = 1, mteMap.width, 1 do
            local properties = mte.getTileProperties({
                locX = j, 
                locY = i, 
                layer = obj.getSolidLayer(mte)
            })
            if properties then
                if properties.solid then
                    grid[i][j] = 1
                else
                    grid[i][j] = 0
                end
            else 
                grid[i][j] = 0
            end
        end
    end
    return grid
end

--Helper function:
--Retrieves the layer with "name"" property as "solid"
obj.getSolidLayer = function (mte)
    local solidLayerNum = 0
    local layers = mte.getLayers()
    for i = 1, #layers, 1 do
        if layers[i].properties.name == "solid" then
            solidLayerNum = i
        end
    end
    return solidLayerNum
end

--[[
Convert the grid into a table of vertices, with each row holding the points of 
a unique shape from the grid. Note: you must pass in a grid with the polygons 
broken up. Removing this limitation is on the to-do list!
]]
obj.getVertexTable = function (grid)
    
    --Turn each object into a vector
    local vectors = obj.objectsToVectors(grid, obj.max, obj.tileSize)
    
    --A perimeter is drawn around each vector
    local perimeters = {}
    for i = 1, #vectors, 1 do
        perimeters[i] = obj.createPerimeters(vectors[i])
    end
    
    --A table of points is built from the perimeters
    return obj.getVertices(perimeters)
end

--[[ 
Create distinct polygon objects on the grid i.e. objects that are not touching. 
E.g. this:

	{0,0,0,0,0,0},
	{0,1,1,0,0,0},
	{0,1,1,0,0,1},
	{1,1,0,1,0,1},
	{0,0,0,1,0,1},
	{0,0,1,1,1,1}
        
Becomes this:

	{0,0,0,0,0,0},
	{0,1,1,0,0,0},
	{0,1,1,0,0,2},
	{1,1,0,2,0,2},
	{0,0,0,2,0,2},
	{0,0,2,2,2,2}

]]
obj.createPolyObjects = function (og) 
    local objNum = 0
    obj.max = 0
    local ng = {}
    for y = 1, #og, 1 do
        ng[y] = {}
        for x = 1, #og[y], 1 do
            local tile = {}
            tile.val = og[y][x];
            tile.x = x
            tile.y = y
            if tile.val == 1 then
                --Tile above
                local tileAbove = {}
                tileAbove.x = x
                tileAbove.y = y-1
                if tileAbove.y > 0 then
                    tileAbove.val = ng[tileAbove.y][tileAbove.x]
                else 
                    tileAbove = nil
                end
                
                --Tile behind
                local tileBehind = {}
                tileBehind.x = x-1
                tileBehind.y = y
                if tileBehind.x > 0 then
                    tileBehind.val = ng[tileBehind.y][tileBehind.x]
                else 
                    tileBehind = nil
                end
                
                --Determine the correct objNum to assign
                if tileAbove == nil or tileAbove.val == 0 then
                    if tileBehind == nil or tileBehind.val == 0 then
                        obj.max = obj.max + 1
                        objNum = obj.max
                    end
                else
                    if tileBehind.val > tileAbove.val then
                        ng = obj.changeObjNums(ng, x, y, objNum, tileAbove.val)
                        objNum = tileAbove.val
                    else
                        objNum = tileAbove.val
                    end
                end
                
                --Populate ng
                ng[y][x] = objNum
            else
                ng[y][x] = 0
            end
        end
    end
    return ng
end

--Helper function:
--Changes the "object number" used on the grid
obj.changeObjNums = function (grid, x, y, oldNum, newNum)
    for j = y, 1, -1 do
        for i = x, 1, -1 do
            if grid[j][i] == oldNum then
                grid[j][i] = newNum
            end
        end
    end
    return grid
end

--[[ 
Breaks up distinct objects. Derives rectangles from larger polygons. E.g. this:

	{0,0,0,0,0,0},
	{0,1,1,0,0,0},
	{0,1,1,0,0,2},
	{1,1,0,2,0,2},
	{0,0,0,2,0,2},
	{0,0,2,2,2,2}
        
Becomes this:

	{0,0,0,0,0,0},
	{0,1,1,0,0,0},
	{0,1,1,0,0,2},
	{3,3,0,4,0,2},
	{0,0,0,4,0,2},
	{0,0,5,4,6,2}

]]
obj.createRectObjects = function (grid)
    local max = obj.max
    local objNum = max
    
    -- Iterate grid
    for y = 1, #grid, 1 do
        for x = 1, #grid[y], 1 do
            
            --Subject tile
            local subject = {}
            subject.val = grid[y][x]
            subject.x = x
            subject.y = y
            
            if subject.val > 0 and subject.val <= max then
                objNum = objNum + 1
                --grid[y][x] = objNum
                
                --Determine width
                local blockWidth = 1
                for i = 1, #grid[y] - x, 1 do
                    if grid[y][x+i] == subject.val then
                        blockWidth = blockWidth + 1
                    else
                        break
                    end
                end
                
                --Determine height
                local blockHeight = 1
                for i = 1, #grid - y, 1 do
                    local invalidRow = false
                    for j = 1, blockWidth, 1 do
                        if grid[y+i][x+j-1] ~= subject.val then
                            invalidRow = true
                            break
                        end
                    end
                    if invalidRow then
                        blockHeight = i
                        break
                    elseif i == (#grid - y) then
                        blockHeight = i
                        break
                    end
                end
                
                --print("objNum: " .. objNum .. ", blockWidth: " .. blockWidth .. ", blockHeight: " .. blockHeight)
                --Stamp the new objNum onto the tiles in the block
                for i = subject.y, subject.y + blockHeight - 1, 1 do
                    for j = subject.x, subject.x + blockWidth -1, 1 do
                        grid[i][j] = objNum
                    end
                end
            end
        end
    end
    
    --Normalise numbers
    for y = 1, #grid, 1 do
        for x = 1, #grid[y], 1 do
            if grid[y][x] ~= 0 then
                grid[y][x] = grid[y][x] - max
            end
        end
    end
    
    obj.max = objNum - max
    return grid
end

--This function creates a table of vectors from the grid, with each vector
--representing one of the shapes on the grid.
obj.objectsToVectors = function (grid, numObjs, tileSize)
    
    --A table to store the vectors
    local vectors = {}
    for i = 1, numObjs, 1 do
        vectors[i] = {}
    end
    
    --Iterate the grid
    for y = 1, #grid, 1 do
        for x = 1, #grid[y], 1 do
            
            --Subject tile properties
            local subject = {}
            subject.x = x
            subject.y = y
            subject.val = grid[y][x]
            
            if subject.val > 0 then
                
                --Check surrounding tiles
                
                --Instantiate
                local tileCheck = {}
                for i = 1, 4, 1 do
                    tileCheck[i] = {}
                end
                
                --1 (Above)
                tileCheck[1].x = x
                tileCheck[1].y = y-1
                if tileCheck[1].x > 0 and tileCheck[1].y > 0 and tileCheck[1].x <= #grid[y] and tileCheck[1].y <= #grid then
                    tileCheck[1].val = grid[tileCheck[1].y][tileCheck[1].x]
                else 
                    tileCheck[1].val = 0
                end
                if tileCheck[1].val ~= subject.val then
                    local vect = {}
                    vect[1] = {(x-1)*tileSize, (y-1)*tileSize}
                    vect[2] = {x*tileSize, (y-1)*tileSize}
                    table.insert(vectors[subject.val], vect)
                end
                
                --2 (Right)
                tileCheck[2].x = x+1
                tileCheck[2].y = y
                if tileCheck[2].x > 0 and tileCheck[2].y > 0 and tileCheck[2].x <= #grid[y] and tileCheck[2].y <= #grid then
                    tileCheck[2].val = grid[tileCheck[2].y][tileCheck[2].x]
                else 
                    tileCheck[2].val = 0
                end
                if tileCheck[2].val ~= subject.val then
                    local vect = {}
                    vect[1] = {x*tileSize, (y-1)*tileSize}
                    vect[2] = {x*tileSize, y*tileSize}
                    table.insert(vectors[subject.val], vect)
                end
                
                --3 (Below)
                tileCheck[3].x = x
                tileCheck[3].y = y+1
                if tileCheck[3].x > 0 and tileCheck[3].y > 0 and tileCheck[3].x <= #grid[y] and tileCheck[3].y <= #grid then
                    tileCheck[3].val = grid[tileCheck[3].y][tileCheck[3].x]
                else 
                    tileCheck[3].val = 0
                end
                if tileCheck[3].val ~= subject.val then
                    local vect = {}
                    vect[1] = {x*tileSize, y*tileSize}
                    vect[2] = {(x-1)*tileSize, y*tileSize}
                    table.insert(vectors[subject.val], vect)
                end
                
                --4 (Left)
                tileCheck[4].x = x-1
                tileCheck[4].y = y
                if tileCheck[4].x > 0 and tileCheck[4].y > 0 and tileCheck[4].x <= #grid[y] and tileCheck[4].y <= #grid then
                    tileCheck[4].val = grid[tileCheck[4].y][tileCheck[4].x]
                else 
                    tileCheck[4].val = 0
                end
                if tileCheck[4].val ~= subject.val then
                    local vect = {}
                    vect[1] = {(x-1)*tileSize, y*tileSize}
                    vect[2] = {(x-1)*tileSize, (y-1)*tileSize}
                    table.insert(vectors[subject.val], vect)
                end				
            end
        end
    end
    return vectors
end

--Draws a perimeter around a supplied vector. Does it by:
--     Provide an unordered table of points
--     Removes inline points
--     Returns a table of clockwise x/y pairs which form a perimeter
obj.createPerimeters = function (points)
    local ordered = {}
    local endPoint = {}
    endPoint.x = points[1][2][1]
    endPoint.y = points[1][2][2]
    ordered[1] = table.remove(points, 1)
    local orderedPointer = 2
    
    --Order points
    local numLoops = 0
    while #points > 1 and numLoops < 500 do
        numLoops = numLoops + 1
        for i = 1, #points, 1 do
            local testPoint = {}
            testPoint.x = points[i][1][1]
            testPoint.y = points[i][1][2]
            if testPoint.x == endPoint.x and testPoint.y == endPoint.y then
                endPoint.x = points[i][2][1]
                endPoint.y = points[i][2][2]
                ordered[orderedPointer] = table.remove(points, i)
                orderedPointer = orderedPointer + 1
                break
            end
        end
    end
    ordered[orderedPointer] = table.remove(points, 1)
    
    --Remove inline points
    for n = 1, #ordered, 1 do			
        if n < #ordered then
            while n < #ordered-1 and obj.areInline(ordered[n][1], ordered[n][2], ordered[n+1][2]) do
                ordered[n][2] = ordered[n+1][2]
                table.remove(ordered, n+1)
            end
        elseif n == #ordered then			
            if obj.areInline(ordered[n][1], ordered[n][2], ordered[1][1]) then
                ordered[n][2] = ordered[1][2]
                table.remove(ordered, 1)
            end
        else
            break
        end
    end
    
    --Flatten
    local flattened = {}
    for i = 1, #ordered, 1 do
        if i < #ordered then
            flattened[i] = ordered[i][1]
        else
            flattened[i] = ordered[i][1]
            flattened[i+1] = ordered[i][2]
        end
    end
    return flattened
end

--Helper function:
--Checks if two points are inline
obj.areInline = function (p1, p2, p3)
    if p1[1] == p2[1] and p2[1] == p3[1] and p1[2] < p2[2] and p2[2] < p3[2] then
        return true
    elseif p1[2] == p2[2] and p2[2] == p3[2] and p1[1] < p2[1] and p2[1] < p3[1] then
        return true
    elseif p1[1] == p2[1] and p2[1] == p3[1] and p1[2] > p2[2] and p2[2] > p3[2] then
        return true
    elseif p1[2] == p2[2] and p2[2] == p3[2] and p1[1] > p2[1] and p2[1] > p3[1] then
        return true
    else
        return false
    end
end

--Create a table of vertices from perimeters
obj.getVertices = function (vectors)
    local rectangles = {}
    for i = 1, #vectors, 1 do
        local lowestX = nil
        local lowestY = nil
        local highestX = nil
        local highestY = nil
        for j = 1, #vectors[i], 1 do
            local pair = vectors[i][j]
            if lowestX == nil or pair[1] < lowestX then
                lowestX = pair[1]
            end
            if highestX == nil or pair[1] > highestX then
                highestX = pair[1]
            end
            if lowestY == nil or pair[2] < lowestY then
                lowestY = pair[2]
            end
            if highestY == nil or pair[2] > highestY then
                highestY = pair[2]
            end
        end
        rectangles[i] = {}
        rectangles[i].width = highestX - lowestX
        rectangles[i].height = highestY - lowestY
        rectangles[i].x = (highestX + lowestX) / 2
        rectangles[i].y = (highestY + lowestY) / 2
    end
    return rectangles
end

--Use for debugging
obj.printGrid = function (grid)
    for y = 1, #grid, 1 do
        local xStr = "{"
        for x = 1, #grid[y], 1 do
            xStr = xStr .. grid[y][x]
            if x < #grid[y] then
                xStr = xStr .. ","
            end
        end
        xStr = xStr .. "}"
        print(xStr)
    end
    print("\n")
end

--Print a vector table
obj.printVectorTable = function (table)
    for row = 1, #table, 1 do
        print("Object " .. tostring(row))
        for i = 1, #table[row], 1 do
            local str = "{" .. tostring(table[row][i][1]) .. "," .. tostring(table[row][i][2]) .. "}"
            print(str)
        end
        print("")	
    end
end

--[[Can probably be removed

obj.vertexArray = function (table)
    local array = {}
    for i = 1, #table, 1 do
        array[i] = {}
        for j = 1, 2*#table[i], 2 do
            array[i][j] = table[i][(j+1)/2][1]
            array[i][j+1] = table[i][(j+1)/2][2]
        end
    end
    return array
end

--Print a vertex array
obj.printVertexArray = function (array)
    for j = 1, #array, 1 do
        print("Object " .. tostring(j))
        for i = 2, 2*#array[j]-2, 2 do
            print(array[j][(i/2)] .. "," .. array[j][(i/2)+1])
        end
    end
end
]]

return obj


