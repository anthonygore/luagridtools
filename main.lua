local mte = require("mte").createMTE()
local gridTools = require('gridTools')
display.setStatusBar( display.HiddenStatusBar )

-- Start physics in MTE
mte.enableBox2DPhysics("all")
mte.physics.start()
mte.physics.setDrawMode("hybrid")

--Load tile set
mte.loadTileSet("red_green", "red_green.jpg");
mte.loadMap("example");

local mteMap = mte.getMap()
local mapHeight = mteMap.height
local mapWidth = mteMap.width

--[[
Create a grid showing the solid tiles in the map e.g.:

    {0,0,0,0,0,0},
    {0,1,1,0,0,0},
    {0,1,1,0,0,1},
    {1,1,0,1,0,1},
    {0,0,0,1,0,1},
    {0,0,1,1,1,1}

This grid can be made elsewhere, but the below function creates it automatically 
from MTE object
]]
local grid = gridTools.createGridFromMTE(mte)
gridTools.printGrid(grid) --for debugging

--Set the tile size of the grid. Presumes square shaped tiles.
gridTools.setTileSize(mteMap.tilewidth)


--[[
Create distinct polygon objects on the grid e.g.:

    {0,0,0,0,0,0},
    {0,1,1,0,0,0},
    {0,1,1,0,0,2},
    {1,1,0,2,0,2},
    {0,0,0,2,0,2},
    {0,0,2,2,2,2}
        
]]
grid = gridTools.createPolyObjects(grid)
gridTools.printGrid(grid) --for debugging

--[[
Breakup polygons on the grid into rectangles on the grid (Box2D can't
use concave objects, easier to just use rectangles) e.g.

    {0,0,0,0,0,0},
    {0,1,1,0,0,0},
    {0,1,1,0,0,2},
    {3,3,0,4,0,2},
    {0,0,0,4,0,2},
    {0,0,5,5,5,5}
        
]]
grid = gridTools.createRectObjects(grid)
gridTools.printGrid(grid) --for debugging

--[[
Convert the grid into a table of vertices, with each row holding the points 
of a unique shape from the grid. Note: you must pass in a grid with the
polygons broken up. Removing this limitation is on the to-do list!
]]
local vertices = gridTools.getVertexTable(grid)

--The table of vertices is then iterated and each object added to MTE as 
--physics objects
for i = 1, #vertices, 1 do
    local rect = display.newRect(0, 0, vertices[i].width, vertices[i].height )
    rect.alpha = 0
    local setup = {
        levelPosX = vertices[i].x,
        levelPosY = vertices[i].y,
        layer =  mte.getSpriteLayer(1),
        name = "solid"
    }
    mte.addSprite(rect, setup)
    mte.physics.addBody(rect, "static", { density=1, friction=0.1, bounce=0.1 } )		
end


--Constrain camera to map
mte.constrainCamera({loc = {1, 1, mapWidth, mapHeight}})

local function gameLoop( event )
    mte.update()
end
Runtime:addEventListener("enterFrame", gameLoop)