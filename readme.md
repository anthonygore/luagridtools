Grid Tools

Lua script to reduce the number of physics bodies added to a Corona SDK project when importing a Tiled project.

Anthony Gore <agor8684@gmail.com>
